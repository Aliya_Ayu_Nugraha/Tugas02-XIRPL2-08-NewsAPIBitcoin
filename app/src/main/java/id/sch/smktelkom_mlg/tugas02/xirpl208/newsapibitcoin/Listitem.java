package id.sch.smktelkom_mlg.tugas02.xirpl208.newsapibitcoin;

/**
 * Created by aliya nugraha on 2/6/2018.
 */

public class Listitem {

    private String head;
    private String desc;

    public Listitem(String head, String desc) {
        this.head = head;
        this.desc = desc;
    }

    public String getHead() {
        return head;
    }

    public String getDesc() {
        return desc;
    }
}
